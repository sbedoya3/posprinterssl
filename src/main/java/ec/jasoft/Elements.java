package ec.jasoft;

import ec.jasoft.view.Tray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Clase para control de los elementos a notificar
 */
public class Elements {

    private static final Logger logger = LoggerFactory.getLogger(Elements.class);

    /**
     * Elementos activos (notificables)
     */
    private static Set<Map<String, Object>> elements = new HashSet<>();
    
    /**
     * Chats activos (notificables)
     */
    private static Set<Map<String, Object>> chats = new HashSet<>();

    /**
     * Se encarga de insertar o actualizar un elemento con estado <tt>Solicitado</tt>
     * Elimina el elemento de la lista (de existir) si lleva otro estado
     * @param data Elemento a procesar
     */
    static void handleEmelent(Map<String, Object> data) {
    	String status = (String) data.get("action");
    	System.out.println("STATUS" + status);
    	if(status!=null)
    		if(status.equals("OK"))
    			return;
    	
    	try{
	        if (data.get("action").equals("autorizar")) {
	            
	        } else {
	            logger.info("Remove if exists in set : {}", data);
	            String aux =  (String) data.get("uid");
	            elements.removeIf(x -> x.get("uid").equals(aux));
	            Tray.notificationSound.stop();
	        }
    	}catch(Exception e) {
    		System.out.println(e);
    	}
    }

   /**
     * Elimina un elemento basado en su uid
     * @param data Elemento a eliminar
     */
    static void removeElement(Map<String, Object> data) {
        elements.removeIf(x -> x.get("uid") == data.get("uid"));
    }
    
    /**
     * El estado si la lista contiene elementos
     * @return El tamano de la lista es mayor a 0
     */
    public static boolean hasElements () {
        return elements.size() > 0;
    }
    
    /**
     * Imprime los elementos en la lista
     */
    public static void imprimir() {
        elements.forEach(x -> {
            x.forEach((k, v) -> System.out.print("[" + k + ": " + v + "] "));
            System.out.println();
        });
    }

    /**
     * Elimina el contenido de la lista de elementos
     */
    static void resetContent() {
        elements = new HashSet<>();
        chats = new HashSet<>();
    }
}
