package ec.jasoft.printer;

import ec.jasoft.SimpleServer;
import ec.jasoft.view.ApplicationPreferences;
import ec.jasoft.view.Tray;
import org.java_websocket.server.DefaultSSLWebSocketServerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.security.KeyStore;

public class PosPrinter {

    public static int port;


    public void startServer() {

        port = Integer.parseInt(ApplicationPreferences.prefs.get(ApplicationPreferences.PORT, "8787"));

        try (InputStream keystoreStream = getClass().getResourceAsStream("/keystore.jks")) {

            // load up the key store
            String STORETYPE = "JKS";

            String STOREPASSWORD = "storepassword";
            String KEYPASSWORD = "keypassword";

            KeyStore ks = KeyStore.getInstance(STORETYPE);
            ks.load(keystoreStream, STOREPASSWORD.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, KEYPASSWORD.toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            SimpleServer server = new SimpleServer(port);
            server.setWebSocketFactory(new DefaultSSLWebSocketServerFactory(sslContext));
            server.start();

            Tray.setTrayIconOk();
            Tray.doNotify("Servicio de impresión inicializado", java.awt.TrayIcon.MessageType.INFO, null);

        } catch (Exception e) {
            Tray.doNotify("Fallo al inicializar servicio de impresión, reintente.", java.awt.TrayIcon.MessageType.ERROR, null);
            Tray.setTrayIconErr();
        }

    }
}
