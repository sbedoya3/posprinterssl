package ec.jasoft.printer;

import com.github.anastaciocintra.output.PrinterOutputStream;

import java.util.List;
import java.util.Map;

public class PrinterDocuments {

    public void printPos(String printer, List<Map<String, Object>> data) throws Exception {

        try (PrinterOutputStream printerOutputStream = new PrinterOutputStream(PrinterOutputStream.getPrintServiceByName(printer))) {
            EscPosExtend escpos = new EscPosExtend(printerOutputStream);

            try {
                escpos.print(data);
            } finally {
                escpos.flush();
                escpos.close();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void printEpson(String printer, List<Map<String, Object>> data) {
        //System.out.println(printer);
        //System.out.println(7);


        try {
            JavaPosPrinter posPrinter = new JavaPosPrinter(printer);

            posPrinter.print(data);
        }catch(Exception ex) {
            //posPrinter.closePrinter();

            ex.printStackTrace();
        }

    }
}
