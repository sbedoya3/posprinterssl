package ec.jasoft.printer;

import jpos.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class JavaPosPrinter {

    private POSPrinterControl114 ptr;
    private CashDrawerControl114 cashDrawer;
    public JavaPosPrinter(String printerName ) {
        // Initialize the POSPrinter
        ptr = (POSPrinterControl114) new POSPrinter();
        cashDrawer=(CashDrawerControl114) new CashDrawer();
        // JavaPOS's code for initialization
        try {
            // Open the device. Use the name of the device that is connected to your computer.
            ptr.open(printerName);
            ptr.claim(10000);
            ptr.setDeviceEnabled(true);



        } catch (JposException ex) {
            ex.printStackTrace();
        }
        try{
            cashDrawer.open("CashDrawer");
            cashDrawer.claim(10000);
            cashDrawer.setDeviceEnabled(true);
        } catch (JposException e){
            System.out.println("sin caja");

        }
    }

    public void print(List<Map<String, Object>> data) {
        try{
        for (Map<String, Object> map : data) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                switch (entry.getKey()) {
                    case "html":
                        this.printHtml(entry.getValue());
                        break;
                    case "openCashBox":

                        break;
                    case "image":

                        break;
                    case "ping":

                        break;
                    default:
                        System.out.println("\t" + entry.getKey() + "/" + entry.getValue());
                        break;
                }
            }
        }

    } finally {
            this.closePrinter();
        }
        }
        /*


                        switch (entry.getKey()) {
                    case "codepage":
                        this.setCodePage(entry.getValue());
                        break;
                    case "text":
                        this.setText(entry.getValue());
                        break;
                    case "newLine":
                        this.setNewLine();
                        break;
                    case "line":
                        this.setLine(entry.getValue());
                        break;
                    case "feed":
                        this.setFeed(entry.getValue());
                        break;
                    case "bold":
                        this.setBold(entry.getValue());
                        break;
                    case "cut":
                        this.setCut(entry.getValue());
                        break;
                    case "align":
                        this.setAlign(entry.getValue());
                        break;
                    case "size":
                        this.setFontSize(entry.getValue());
                        break;
                    case "font":
                        this.setFont(entry.getValue());
                        break;
                    case "resetStyle":
                        this.resetStyle();
                        break;
                    case "openCashBox":
                        this.openCashBox();
                        break;
                    case "image":
                        this.setImage(entry.getValue());
                        break;
                    case "ping":
                        this.pingPrinter();
                        break;
                    default:
                        System.out.println("\t" + entry.getKey() + "/" + entry.getValue());
                        break;
                }
         */

    private void printHtml(Object value) {
        Map<String, String> htmlMap = (Map<String, String>) value;
        String htmlString = htmlMap.get("element");
        //System.out.println(value);
        Map<String, List<String>> textGroups = convertHtmlToText(htmlString);
        //System.out.println(textGroups);
       // printSection(textGroups, "header");
       // printSection(textGroups, "orderlines");
        printSection(textGroups, "totals");
        //printSection(textGroups, "footer");

        for (int i = 0; i < 5; i++) {
//            this.printString("");
            System.out.println();
        }

    }
    private void beep(){
        try{
            byte[] beepCommand ={0x007};
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, new String(beepCommand, "UTF-8"));
        }catch (UnsupportedEncodingException | JposException ex ){
           ex.printStackTrace();
        }



        }
    private void printSection(Map<String, List<String>> textGroups, String section) {
        if (textGroups.containsKey(section)) {
            if ("orderlines".equals(section)) {
                List<String> lines = new ArrayList<>(textGroups.get(section));
                for (int i = 0; i < lines.size(); i++) {
                    String item = lines.get(i);
                    String quantityAndPrice = "";
                    String total = "";
                    if (i + 1 < lines.size()) {
                        quantityAndPrice = lines.get(i + 1);
                        i++;
                    }
                    if (i + 1 < lines.size() && quantityAndPrice.contains("x") && quantityAndPrice.contains("$")) {
                        total = lines.get(i + 1);
                        i++;
                    }
                    this.printStringOrderLines(item, quantityAndPrice, total);
                }
                this.printBlanckLines(2);
            } else if ("header".equals(section)) {
                String receipt = String.join("\n", textGroups.get(section));
                String[] parts = receipt.split("Clave de acceso:");
                String section1 = parts[0].trim();

                if (parts.length > 1) {
                    String section2 = "Clave de acceso:" + parts[1].trim();
                    for (String line : section1.split("\n")) {
                        printStringHeader(line, "center");
                    }
                    this.printBlanckLines(2);
                    String[] lines = section2.split("\n");
                    boolean combine = false;
                    for (int i = 0; i < lines.length; i++) {
                        if (lines[i].startsWith("Fecha:")) {
                            combine = true;
                        }
                        if (combine && i + 1 < lines.length) {
                            String combinedLine = lines[i].trim() + " " + lines[i + 1].trim();
                            printStringHeaderleft(combinedLine, "left");
                            i++;
                        } else {
                            printStringHeaderleft(lines[i], "left");
                        }
                    }

                } else {
                    for (String line : section1.split("\n")) {
                        printStringHeader(line, "center");
                    }
                }
                this.printBlanckLines(2);
            }else if ("totals".equals(section)){
                /*

               List<String> lines= textGroups.get(section);
                for(int i=0; i< lines.size(); i++){
                    String line1=lines.get(i);
                    if(line1.trim().equals("--------") || line1.trim().equals("Forma de pago:")){
                        System.out.println(line1);
                    }else {
                     String line2=  i+1 < lines.size() ? lines.get(i+1):"";
                        System.out.println(line1+" "+line2);
                        i++;
                    }
                }
                    */

                List<String> lines = textGroups.get(section);

                for (int i = 0; i < lines.size(); i++) {
                    String line1= lines.get(i);
                    String line2= i+1 <lines.size() ? lines.get(i+1):" ";
                    if(!line1.trim().equals("--------") && !line1.trim().equals("Forma de pago:")){
                        this.printTotalString(line1,line2);
                        i++;
                    }else{
                        this.printTotalString(line1,"");
                    }

                }
                this.printBlanckLines(2);
                /*for(String line :textGroups.get(section)){
                    System.out.println(line);
                   // this.printString(line);
                }*/
            }else if("footer".equals(section)){
                for (String line : textGroups.get(section)) {
                    //System.out.println(line);
                    this.printStringFooter(line);
                 }
                this.printBlanckLines(5);
                //this.beep();
            }else{

            }

//            for (String line : textGroups.get(section)) {
//                System.out.println(line);
////                this.printString(line);
//            }

//            this.printString("");
        }
    }


    public static Map<String, List<String>> convertHtmlToText(String html) {
        Map<String, List<String>> textGroups = new HashMap<>();
        String currentSection = null;

        // Parse HTML using JSoup
        Document doc = Jsoup.parse(html);

        // Extract text from each element
        extractTextFromElement(doc.body(), textGroups, currentSection);

        return textGroups;
    }


    private static void extractTextFromElement(Element element, Map<String, List<String>> textGroups, String currentSection) {
        for (Node node : element.childNodes()) {
            if (node instanceof TextNode) {
                // Text node, add text to the current section
                addToGroup(textGroups, currentSection, node.toString().trim());
            } else if (node instanceof Element) {
                Element child = (Element) node;

                if ("div".equalsIgnoreCase(child.tagName())) {
                    // Div element, check for specific classes to determine the section
                    String divClass = child.className();
                    if (divClass.contains("pos-receipt-contact")) {
                        currentSection = "header";
                    } else if (divClass.contains("orderlines")) {
                        currentSection = "orderlines";
                    } else if (divClass.contains("pos-receipt-right-align") && child.text().contains("--------")) {
                        currentSection = "totals";
                    } else if (divClass.contains("after-footer")) {
                        currentSection = "footer";
                    }

                    // Recursive call for child elements
                    extractTextFromElement(child, textGroups, currentSection);
                } else {
                    // Recursive call for other elements
                    extractTextFromElement(child, textGroups, currentSection);
                }
            }
        }
    }

    private static void addToGroup(Map<String, List<String>> textGroups, String group, String text) {
        if (!textGroups.containsKey(group)) {
            textGroups.put(group, new ArrayList<>());
        }
        textGroups.get(group).add(text);
    }

    public void printBlanckLines(int numlines){
        try{
            for (int i = 0; i <numlines ; i++) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,"\n");
            }
        }catch (JposException ex){
                ex.printStackTrace();
        }
    }


    public void printStringOrderLines(String item, String quantityAndPrice, String total) {
        // Código de JavaPOS para imprimir
        try{
            ptr.setRecLineChars(39);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, item + "\n");
            if(!quantityAndPrice.isEmpty() && !total.isEmpty()) {
                String formatted = String.format("%-30s%s", quantityAndPrice, total);
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, formatted + "\n");
            } else {
                if(!quantityAndPrice.isEmpty()) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,String.format("%39s",quantityAndPrice)  + "\n");
                }
                if(!total.isEmpty()) {
                    String formatted = String.format("%39s-", total);
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, formatted + "\n");
                }
            }
        } catch (JposException ex){
            ex.printStackTrace();
        }
    }

    public void printTotalString(String line1,String line2){
        try {
            ptr.setRecLineChars(39);
            if(line1.trim().equals("--------")||line1.trim().equals("Forma de pago:")){
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,line1 +"\n");


            }else{
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,String.format("%-10s %10s",line1, line2 +"\n"));
            }


        } catch (JposException ex) {
            ex.printStackTrace();
        }
        // printNormal(int station, String data)
        // A string is sent using the method "printNormal", and it is printed.
        // "\n" is the standard code for starting a new line.
        // When the end of the line has no "\n", printing using the method "printNormal" doesn't start, maybe.
        //+ "\u001b|cA"

    }

    public void printStringFooter(String text){
        try {
            int lineLength = 40;
            ptr.setRecLineChars(lineLength);
            String[] words = text.split(" ");
            StringBuilder line = new StringBuilder(words[0]);

            for (int i = 1; i < words.length; i++) {
                if (line.length() + words[i].length() > lineLength) {
                    formatLine(spaceCenterText(line.toString(), lineLength));
                    line = new StringBuilder(words[i]);
                } else if (line.length() + words[i].length() + 1 > lineLength) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, spaceCenterText(line.toString(), lineLength) + "\n");
                    line = new StringBuilder(words[i]);
                } else {
                    line.append(" ").append(words[i]);
                }
            }
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, spaceCenterText(line.toString(), lineLength) + "\n");
        } catch (JposException ex) {
            ex.printStackTrace();
        }
    }


    public String spaceCenterText(String text, int lineLength){
        int start=(lineLength- text.length())/2;
        int end = lineLength-start-text.length();
        return repeat(" ",start)+ text+ repeat(" ",end);

    }
    public String repeat(String str, int times){
        StringBuilder sb= new StringBuilder();
        for (int i = 0; i < times; i++) {
            sb.append(str);
        }
        return sb.toString();
    }
    public void formatLine(String line)throws JposException{
        int colonIndex = line.indexOf(":");
        if(colonIndex !=-1 && !line.matches(".*\\d{4}\\s\\d{2}:\\d{2}:\\d{2}.*")){
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,"\u001bE"+line.substring(0,colonIndex+1)+"\u001bF" + line.substring(colonIndex+1)+"\n");
            //System.out.println("\u001bE"+line.substring(0,colonIndex+1)+"\u001bF" + line.substring(colonIndex+1)+"\n");
        }else{
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,line+"\n");
            //System.out.println(line+"\n");
        }
    }
    public void printStringHeader(String text, String alignment) {
        try {
            int lineLength = 40;
            ptr.setRecLineChars(lineLength);
            String[] words = text.split(" ");
            StringBuilder line = new StringBuilder(words[0]);
            for (int i = 1; i < words.length; i++) {
                if (line.length() + words[i].length() > lineLength) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, formatLineHeader(line.toString(), lineLength, alignment) + "\n");
                    line = new StringBuilder(words[i]);
                } else {
                    line.append(" ").append(words[i]);
                }
            }
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, formatLineHeader(line.toString(), lineLength, alignment) + "\n");
        } catch (JposException ex) {
            ex.printStackTrace();
        }
    }
    public void printStringHeaderleft(String text, String alignment) {
        try {
            int lineLength = 40;
            ptr.setRecLineChars(lineLength);
            if ("left".equals(alignment)) {
                // Si la alineación es a la izquierda, imprime la línea tal cual
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, text + "\n");
            } else {
                // Si la alineación no es a la izquierda (es decir, es "center"), divide la línea como antes
                String[] words = text.split(" ");
                StringBuilder line = new StringBuilder(words[0]);
                for (int i = 1; i < words.length; i++) {
                    if (line.length() + words[i].length() > lineLength) {
                        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, formatLineHeader(line.toString(), lineLength, alignment) + "\n");
                        line = new StringBuilder(words[i]);
                    } else {
                        line.append(" ").append(words[i]);
                    }
                }
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, formatLineHeader(line.toString(), lineLength, alignment) + "\n");
            }
        } catch (JposException ex) {
            ex.printStackTrace();
        }
    }

    public String formatLineHeader(String text, int lineLength, String alignment){
        if ("center".equals(alignment)){
            int start = (lineLength-text.length())/2;
            int end =lineLength - start - text.length();
            return repeatheader(" ", start) + text + repeatheader(" ", end);
        }else{
            return text + repeatheader(" ", lineLength - text.length());
        }
    }
    public String repeatheader(String str,int times){
        StringBuilder sb= new StringBuilder();
        for(int i = 0; i<times; i++){
            sb.append(str);
        }
        return sb.toString();
    }
/*
    public String formatLine(String text, int lineLength, String alignment) {
        if ("center".equals(alignment)) {
            int start = (lineLength - text.length()) / 2;
            int end = lineLength - start - text.length();
            return repeat(" ", start) + text + repeat(" ", end);
        } else { // left alignment
            return text + repeat(" ", lineLength - text.length());
        }
    }

    public String repeat(String str, int times) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < times; i++) {
            sb.append(str);
        }
        return sb.toString();
    }
*/
    /**
     * Close the POSPrinter and release resources.
     */
    public void closePrinter() {
        // JavaPOS's code for closing
        try {
            if (cashDrawer != null && cashDrawer.getState() != JposConst.JPOS_S_CLOSED){
                cashDrawer.openDrawer();
                cashDrawer.setDeviceEnabled(false);
                cashDrawer.release();
                cashDrawer.close();

            }
            // Disable the device.
            ptr.setDeviceEnabled(false);

            // Release the device exclusive control right.
            ptr.release();

            // Finish using the device.
            ptr.close();
        } catch (JposException ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args) {
        // Example usage
        JavaPosPrinter posPrinter = new JavaPosPrinter("POSPrinter1");
        LinkedHashMap<String, String> htmlReceipt = new LinkedHashMap<>();

        htmlReceipt.put("element","element=<html xmlns=\"http://www.w3.org/1999/xhtml\"><head></head><body><div class=\"pos-receipt\"><br /><div class=\"pos-receipt-contact\"><strong class=\"company\">DISPEQPRUEBAS</strong><span>Matriz: JAIME ROLDOS2</span><br /><span>OBLIGADO A LLEVAR CONTABILIDAD: NO</span><br /><div>admin@email.com</div><div class=\"invoice_section\"><br /><div><div style=\"text-align:center;\"><b><span>FACTURA: </span>001-001-000000001</b><br /></div></div><div class=\"electronic_invoice\" style=\"padding-top:2px; font-size: 80%\"><div>Clave de acceso:</div><div style=\"word-wrap: break-word;\">1502202401false1001001000000001944713471NaN </div></div></div><div class=\"client_section\" style=\"padding-top: 5px;\"><div><b>Fecha: </b>2024-02-15 17:56:06-05:00</div><div><b>Nombre:</b>Sebastian Bedoya</div><div><strong>Ced/RUC:</strong>0106481914</div><div><div><strong>E-mail:</strong>sebas120720@gmail.com</div><div><strong>Dirección:</strong>camino del tejar</div></div></div></div><br /><br /><div class=\"orderlines\"><div class=\"responsive-price\">Almeja en pulpa<span class=\"price_display pos-receipt-right-align\">3,50</span></div><span></span><div class=\"responsive-price\">Almeja entera en cáscara<span class=\"price_display pos-receipt-right-align\">2,50</span></div><span></span><div>Aros de calamar</div><span></span><div class=\"pos-receipt-left-padding\">2 x $ 4,00<span class=\"price_display pos-receipt-right-align\">8,00</span></div></div><div class=\"pos-receipt-right-align\">--------</div><br /><div class=\"pos-receipt-right-align\" style=\"display: contents; font-size: 16px;\"><div class=\"pos-receipt-amount\"><span style=\"font-size: 16px;\"> Subtotal</span><span class=\"pos-receipt-right-align\" style=\"font-size: 16px;\">14,00</span></div></div><br /><div class=\"pos-receipt-amount\"> TOTAL <span class=\"pos-receipt-right-align\">$ 14,00</span></div><br /><br /><strong>Forma de pago:</strong><div>Efectivo<span class=\"pos-receipt-right-align\">20,00</span></div><br /><div class=\"pos-receipt-amount receipt-change\"> CAMBIO <span class=\"pos-receipt-right-align\">$ 6,00</span></div><br /><div class=\"before-footer\"></div><div class=\"after-footer\"></div><br /><div class=\"pos-receipt-order-data\"><div>Pedido 00010-001-0001</div><div>2024-02-15 17:56:06-05:00</div><div class=\"cashier\"><div>Atendido por CERVIO ARCEDIANO BRITO BRITO</div></div></div></div></body></html>}\n");
                posPrinter.printHtml(htmlReceipt);

        posPrinter.closePrinter();


    }


}

