package ec.jasoft.printer;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MessageMap {

    public static Map<String, Object> decode(String json) {
        try {
            return new ObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {
            });

        } catch (Exception e) {
            return new HashMap<String, Object>();
        }
    }

    public static String encode(Map map) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(map);
        } catch (IOException e) {
            return "-";
        }
    }
}
