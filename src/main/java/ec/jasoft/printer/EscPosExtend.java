package ec.jasoft.printer;

import com.github.anastaciocintra.escpos.EscPos;
import com.github.anastaciocintra.escpos.Style.FontName;
import com.github.anastaciocintra.escpos.Style.FontSize;
import com.github.anastaciocintra.escpos.image.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class EscPosExtend extends EscPos {

    public EscPosExtend(OutputStream outputStream) {
        super(outputStream);
    }

    public void print(List<Map<String, Object>> data) throws Exception {

        for (Map<String, Object> map : data) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                switch (entry.getKey()) {
                    case "codepage":
                        this.setCodePage(entry.getValue());
                        break;
                    case "text":
                        this.setText(entry.getValue());
                        break;
                    case "newLine":
                        this.setNewLine();
                        break;
                    case "line":
                        this.setLine(entry.getValue());
                        break;
                    case "feed":
                        this.setFeed(entry.getValue());
                        break;
                    case "bold":
                        this.setBold(entry.getValue());
                        break;
                    case "cut":
                        this.setCut(entry.getValue());
                        break;
                    case "align":
                        this.setAlign(entry.getValue());
                        break;
                    case "size":
                        this.setFontSize(entry.getValue());
                        break;
                    case "font":
                        this.setFont(entry.getValue());
                        break;
                    case "resetStyle":
                        this.resetStyle();
                        break;
                    case "openCashBox":
                        this.openCashBox();
                        break;
                    case "image":
                        this.setImage(entry.getValue());
                        break;
                    case "ping":
                        this.pingPrinter();
                        break;
                    default:
                        System.out.println("\t" + entry.getKey() + "/" + entry.getValue());
                        break;
                }
            }
        }
    }

    private void setCodePage(Object value) throws IllegalArgumentException, IOException {
        String codePage = (String) value;
        this.setCharacterCodeTable(CharacterCodeTable.valueOf(codePage));

    }

    private void setFont(Object value) {
        String fontName = (String) value;
        this.getStyle().setFontName(FontName.valueOf(fontName));
    }

    private void resetStyle() {
        this.getStyle().reset();
    }

    /**
     * Revisar en:
     * https://help.aronium.com/hc/en-us/articles/115002486449-Cash-drawer
     * http://keyhut.com/popopen.htm
     */
    private void openCashBox() throws IOException {
        this.write(27);
        this.write(112);
        this.write(0);
        this.write(25);
        this.write(250);
        System.out.println("---OPEN CASH BOX---");
    }

    private void pingPrinter() throws IOException {
        System.out.println("****BEEEP****");
        this.write(27);
        this.write(66);
        this.write(2); //number of beeps [1-9]
        this.write(1); //lenght of beep [1-9]
    }

    private void setLine(Object value) throws UnsupportedEncodingException, IOException {
        Map<String, String> entry = (Map<String, String>) value;
        String text = entry.get("value");
        this.writeLF(text);
    }

    private void setImage(Object value) throws IOException {
        Map<String, String> entry = (Map<String, String>) value;

        String base64string = entry.get("element");
        byte[] image = Base64.getDecoder().decode(base64string);
        Bitonal algorithm = new BitonalThreshold(127);

        InputStream is = new ByteArrayInputStream(image);
        BufferedImage newBi = ImageIO.read(is);

        int b = (int) (Math.random() * 101);
        File outputfile = new File("\\\\VBOXSVR\\win_10_share\\impresiones odoo\\odooprinter" + b + ".jpg");
        ImageIO.write(newBi, "jpg", outputfile);

        EscPosImage escposImage = new EscPosImage(new CoffeeImageImpl(newBi), algorithm);

        BitImageWrapper imageWrapper = new BitImageWrapper();
        imageWrapper.setJustification(Justification.Center);
//        this.write(imageWrapper, escposImage);

        System.out.println("---PRINTS ON PAPER---");
    }

    private void setFeed(Object value) throws IllegalArgumentException, IOException {
        int nLines = (int) value;
//        this.feed(nLines);
    }

    private void setBold(Object value) {
        boolean bold = (boolean) value;
        this.getStyle().setBold(bold);
    }

    private void setAlign(Object value) {
        String align = (String) value;
        this.getStyle().setJustification(Justification.valueOf(align));
    }

    private void setCut(Object value) throws IOException {
        String cutMode = (String) value;
//        this.cut(CutMode.valueOf(cutMode));
        System.out.println("---CUTS PAPER---");
    }

    private void setNewLine() throws IOException {
        this.write(EscPos.LF);
    }

    private void setText(Object data) throws UnsupportedEncodingException, IOException {
        Map<String, String> entry = (Map<String, String>) data;
        String text = entry.get("value");
        this.write(text);
    }

    private void setFontSize(Object data) {
        Map<String, String> entry = (Map<String, String>) data;
        String fontWidth = entry.get("fontWidth");
        String fontHeight = entry.get("fontHeight");
        this.getStyle().setFontSize(FontSize.valueOf(fontWidth), FontSize.valueOf(fontHeight));
    }

}
