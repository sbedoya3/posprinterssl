package ec.jasoft.listener;

import ec.jasoft.Application;
import ec.jasoft.view.Tray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Evento llamado al finalizar el temporizador de recordatorio
 */
public class RestartListener implements ActionListener {

    private final Logger logger = LoggerFactory.getLogger(RestartListener.class);

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("errorrorororo");

        try {
            restartApplication();
            Tray.setTrayIconOk();
        } catch (Exception ex) {
            logger.error("Error al reiniciar websocket connection");
            ex.printStackTrace();
        }
    }

    public void restartApplication() throws URISyntaxException, IOException {
        final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        final File currentJar = new File(Application.class.getProtectionDomain().getCodeSource().getLocation().toURI());

        String arc = currentJar.getPath().replaceAll(".exe", ".jar");

        // Build command: java -jar application.jar
        final ArrayList<String> command = new ArrayList<String>();
        command.add(javaBin);
        command.add("-jar");
        command.add("" + arc);

        final ProcessBuilder builder = new ProcessBuilder(command);
        builder.start();
        System.exit(0);
    }
}
