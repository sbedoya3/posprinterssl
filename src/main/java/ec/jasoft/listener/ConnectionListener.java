package ec.jasoft.listener;

import ec.jasoft.view.ApplicationPreferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Accion asignada al clic en el elemento de menu <tt>Configuracion</tt>
 */
public class ConnectionListener implements ActionListener {

    private final Logger logger = LoggerFactory.getLogger(ConnectionListener.class);

    /**
     * Abre la ventana de configuracion. Luego, reinicia el listener
     * @param e Evento semantico al ocurrir la accion
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        boolean status = ApplicationPreferences.showConfigDialog();
        if(status==true) {
        	try { 
                //FirebaseConnection.close();
                //FirebaseConnection.init();
            } catch (Exception ex) {
                logger.error("Error al reiniciar firebase connection");
                ex.printStackTrace();
            }
        }else {
        	/*try {
                FirebaseConnection.close();
            } catch (Exception ex) {
                logger.error("Error al reiniciar firebase connection");
                ex.printStackTrace();
            }*/
        }
        
    }
}
