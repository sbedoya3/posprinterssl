package ec.jasoft.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener to quit app
 */
public class ExitListener implements ActionListener {

    private final Logger logger = LoggerFactory.getLogger(ExitListener.class);

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.info("Saliendo del notificador");
        System.exit(0);
    }

}
