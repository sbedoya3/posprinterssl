package ec.jasoft;

import ec.jasoft.printer.PosPrinter;
import ec.jasoft.view.ApplicationPreferences;
import ec.jasoft.view.Tray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

public class Application {
    private final Logger logger = LoggerFactory.getLogger(Application.class);

    /**
     * Initialize Tray and WebSocket connection
     */
    private Application() {
        logger.info("Initializing application");

        try {
            new Tray();
            logger.info("Establishing connection...");
            PosPrinter printer = new PosPrinter();
            printer.startServer();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
            logger.error("Error while trying to establish connection: {}", e.getMessage());
        }
    }

    /**
     * Check if there are saved preferences or request them
     *
     * @param args does not take args
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(Application::initializeApplication);
    }

    private static void initializeApplication() {
        ApplicationPreferences applicationPreferences = new ApplicationPreferences();
        if (!applicationPreferences.isConfigured()) {
            boolean configurationStatus = ApplicationPreferences.showConfigDialog();
            if (configurationStatus) {
                if (applicationPreferences.isConfigured()) {
                    Application application = new Application();
                }
            }
        } else {
            Application application = new Application();
        }
    }
}
