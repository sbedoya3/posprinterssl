package ec.jasoft.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Evento principal, responde al clic de la notificacion asi como del icono en Taskbar
 */
public class TrayIconListener implements ActionListener {

    private final Logger logger = LoggerFactory.getLogger(TrayIconListener.class);
    private final TrayIcon trayIcon;

    public TrayIconListener(TrayIcon trayIcon) {
        this.trayIcon = trayIcon;
    }

    /**
     * Verifica si hay soporte para abrir la pagina web, de ser asi procede.
     * Detiene el sonido de notificacion
     *
     * @param e Evento semantico que indica que una accion ocurrio
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Elements.imprimir();
        Tray.notificationSound.stop();
        /*try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                //Desktop.getDesktop().browse(new URI(ApplicationPreferences.URL_DIRECT));
            }
        } catch (URISyntaxException ex) {
            logger.error("Error en sintaxis de URI");
            ex.printStackTrace();
        } catch (IOException ex) {
            logger.error("Error IO al intentar abrir la URI");
            ex.printStackTrace();
        }*/

    }
}
