package ec.jasoft.view;

import ec.jasoft.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.prefs.Preferences;

/**
 * Preferencias generales de la aplicacion
 */
public class ApplicationPreferences {

    private final Logger logger = LoggerFactory.getLogger(ApplicationPreferences.class);

    /**
     * Nombre de campo para el tiempo de verificación de conexion por {@link Preferences}
     */
    public static final String PORT = "ping_time";

    /**
     * Nombre de campo para el emial de usuario de la base conectado {@link Preferences}
     */
    public static final String HOST = "user_id";

    /**
     * Contrasena estatica de administracion
     */
    private static final String ADMIN_SECRET = "sn0w";

    /**
     * Preferencias a nivel de usuario
     */
    public static Preferences prefs = Preferences.userRoot().node(Application.class.getName());

    /**
     * Abre ventana de seguridad, luego una de configuracion
     */
    public static boolean showConfigDialog() {

        JTextField portTextField = new JTextField(prefs.get(PORT, "8787"));

        Object[] input = {
                "Puerto de comunicación:", portTextField,
        };

        int option = JOptionPane.showConfirmDialog(
                null, input, "Configuración", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            int port = Integer.parseInt(portTextField.getText());

            prefs.putInt(PORT, port);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Revisa si se ha configurado la aplicacion
     *
     * @return Devuelve si existen valores asignados a los parametros descritos en esta clase
     */
    public boolean isConfigured() {
        return prefs.getInt(PORT, 0) != 0;

    }
}
