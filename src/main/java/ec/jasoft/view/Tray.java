package ec.jasoft.view;

import ec.jasoft.Application;
import ec.jasoft.listener.ConnectionListener;
import ec.jasoft.listener.ExitListener;
import ec.jasoft.listener.RestartListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Objects;

public class Tray {

    private final Logger logger = LoggerFactory.getLogger(Tray.class);

    private static SystemTray tray;
    private static TrayIcon trayIcon;

    private static Image CHECK_IMAGE;
    private static Image NO_IMAGE;

    public static Clip notificationSound;

    public static MenuItem resetItem = null;


    /**
     * Carga de elementos multimedia. Verificacion de soporte de SO para Tray.
     * Inicializa Tray y agrega eventos a sus componentes
     *
     * @throws Exception
     */
    public Tray() throws Exception {
        try {
            CHECK_IMAGE = ImageIO.read(Objects.requireNonNull(
                    Application.class.getClassLoader().getResourceAsStream("images/check.png")));
            NO_IMAGE = ImageIO.read(Objects.requireNonNull(
                    Application.class.getClassLoader().getResourceAsStream("images/no.png")));
        } catch (IOException e) {
            logger.error("¡No se pudo encontrar los recursos de imagen!");
            System.exit(1);
        }

        try {
            InputStream audioSrc = getClass().getResourceAsStream("/chat2.wav");
            InputStream bufferedIn = new BufferedInputStream(audioSrc);

            AudioInputStream audioIn = AudioSystem.getAudioInputStream(bufferedIn);
            notificationSound = AudioSystem.getClip();
            notificationSound.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            logger.error("Archivo de audio no soportado!");
        } catch (IOException e) {
            logger.error("Error al procesar archivo de audio para notificacion");
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            logger.error("Error al tomar dato getClip");
            e.printStackTrace();
        } catch (Exception e) {
            logger.error("Error al tomar dato getClip");
            e.printStackTrace();
            throw new Exception("Error al cargar audios de notificaciones");
        }

        if (SystemTray.isSupported()) {
            logger.info("System tray supported");
            tray = SystemTray.getSystemTray();

            logger.info("Creando iconos de bandeja...");
            final PopupMenu popup = new PopupMenu();

            final MenuItem lbl = new MenuItem("POS Printer - Jasoft SSL");
            lbl.setEnabled(false);
            popup.add(lbl);

            popup.addSeparator();

            final MenuItem configItem = new MenuItem("Configuracion");
            configItem.addActionListener(new ConnectionListener());
            popup.add(configItem);


            popup.addSeparator();

            resetItem = new MenuItem("Reiniciar servicio");
            resetItem.addActionListener(new RestartListener());
            popup.add(resetItem);

            popup.addSeparator();

            final MenuItem exitItem = new MenuItem("Salir");
            exitItem.addActionListener(new ExitListener());
            popup.add(exitItem);

            trayIcon = new TrayIcon(NO_IMAGE, "El Notificador no esta listo todavía", popup);

            final ActionListener tooltipListener = new TrayIconListener(trayIcon);
            trayIcon.addActionListener(tooltipListener);

            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                logger.error("Error al agregar TrayIcon a la bandeja de sistema");
                e.printStackTrace();
            }

        } else {
            logger.error("SYSTEM TRAY NOT SUPPORTED");
        }
    }

    /**
     * Cambio de icono a Conectado
     */
    public static void setTrayIconOk() {
        trayIcon.setToolTip("Servicio: Activado");
        trayIcon.setImage(CHECK_IMAGE);
    }

    /**
     * Cambio de icono a Desconectado
     */
    public static void setTrayIconErr() {
        trayIcon.setToolTip("Servicio: Desconectado");
        trayIcon.setImage(NO_IMAGE);
    }


    /**
     * Enviar notificacion al sistema, accionar sonido y controlar/reiniciar temporizador de recordatorio
     *
     * @param texto Texto de la notificacion
     * @param tipo  Tipo de notificacion {@link TrayIcon.MessageType}
     */
    public static void doNotify(String texto, TrayIcon.MessageType tipo, Map<String, Object> data) {
        trayIcon.displayMessage("PosPrinter Jasoft", texto, tipo);
        notificationSound.setFramePosition(0);
        notificationSound.start();
    }

    public static void doNotifyAlert() {
        notificationSound.setFramePosition(0);
        notificationSound.start();
    }


}
