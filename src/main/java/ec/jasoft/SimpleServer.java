package ec.jasoft;

import ec.jasoft.printer.MessageMap;
import ec.jasoft.printer.PrinterDocuments;
import ec.jasoft.view.Tray;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleServer extends WebSocketServer {

    public SimpleServer(int port) {
        super(new InetSocketAddress(port));
    }

//    public static void main(String[] args) {
//        int port = 8786;
//        SimpleServer server = new SimpleServer(new InetSocketAddress(port));
//        server.run();
//    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        webSocket.send("{\"action\": \"response\", \"message\": \"welcome to the server\"}");
        broadcast("{\"action\": \"response\", \"message\": \"" + "new connection: " + clientHandshake.getResourceDescriptor() + "\"}");
        System.out.println("new connection to " + webSocket.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket webSocket, int code, String reason, boolean remote) {
        System.out.println("closed " + webSocket.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(WebSocket webSocket, String message) {
        System.out.println("received message from " + webSocket.getRemoteSocketAddress());

        Map<String, Object> map = MessageMap.decode(message);

        if (map.size() == 0) {
            webSocket.send("{\"action\": \"response\", \"message\": \"" + message + "\"}");
            return;
        }

        String action = (String) map.get("action");
        String type = (String) map.get("type");
        System.out.println(type);
        if ("print".equals(action) && "img".equals(type)) {
            printNormal(map, webSocket);
        } else if ("print".equals(action) && "html".equals(type)) {
            printHtml(map, webSocket);
        } else {
            System.out.println("{\"action\": \"response\", \"message\": \"no action defined\"}");
            webSocket.send("{\"action\": \"response\", \"message\": \"no action defined\"}");
        }

    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        System.out.println("received ByteBuffer from " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        System.err.println("an error occurred on connection " + webSocket.getRemoteSocketAddress() + ":" + e);
    }

    @Override
    public void onStart() {
        System.out.println("server started successfully");
    }


    public void printNormal(Map<String, Object> data, WebSocket conn) {

        Map<String, Object> response = new HashMap<>();

        try {
            PrinterDocuments pd = new PrinterDocuments();
            pd.printPos((String) data.get("printer"), (List<Map<String, Object>>) data.get("document"));
            response.put("action", "print");
            response.put("code", 1);
            response.put("message", "impresion satisfactoria");

        } catch (Exception e) {
            e.printStackTrace();
            response.put("action", "print");
            response.put("code", 99);
            response.put("message", "error en impresion, " + e.getMessage());
        }

        try {
            boolean alert = (Boolean) data.get("alert");
            if (alert)
                Tray.doNotifyAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        conn.send(MessageMap.encode(response));
    }

    public void printHtml(Map<String, Object> data, WebSocket conn) {

        Map<String, Object> response = new HashMap<>();

        try {
            PrinterDocuments pd = new PrinterDocuments();
            pd.printEpson((String) data.get("printer"), (List<Map<String, Object>>) data.get("document"));
            response.put("action", "print");
            response.put("code", 1);
            response.put("message", "impresion satisfactoria");

        } catch (Exception e) {
            e.printStackTrace();
            response.put("action", "print");
            response.put("code", 99);
            response.put("message", "error en impresion, " + e.getMessage());
        }

        try {
            boolean alert = (Boolean) data.get("alert");
            if (alert)
                Tray.doNotifyAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        conn.send(MessageMap.encode(response));
    }
}